package com.bh.spring.forms.springforms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringFormsApplication {

	public static void main(String[] args) throws Exception {

		SpringApplication.run(SpringFormsApplication.class, args);
	}

}

